//рисование в окне с кадрами
#include "PaintScene.h"



PaintScene::PaintScene(QObject* parent) : QGraphicsScene(parent)
{
    
}

PaintScene::~PaintScene()
{

}

bool PaintScene::IsSelected()                                                                       // Проверяет есть ли выбранный объект
{
    if (selected_object == nullptr) {
        return false;
    }
    return true;
}

void PaintScene::SetSelectedObject(QGraphicsItem* new_selected_object)
{
    for (int i = 0; i < polygons_on_scene.size(); i++)
    {
        if (new_selected_object == polygons_on_scene[i]) {
            selected_object = &(*polygon_pointer_vector[i]);
            break;
        }
    }   
}

void PaintScene::PointDrawing(double x, double y) {
    QRectF single_ellipse(x, y, 5.0, 5.0);
    single_ellipse_vector.push_back(single_ellipse);
    for (int i = 0; i < single_ellipse_vector.size(); i++)
    {
        addEllipse(single_ellipse_vector[i], QPen(Qt::NoPen), QBrush(Qt::green));       
    }   
}

/*
void PaintScene::PolygonDrawing(double x, double y) {
    if (!IsSelected()) {
        // Сделать выделение текущего полигона 
        CustomPolygon polygon_object;
        polygon_object.polygon_coordinates.push_back(QPointF(x,y));
        polygon_vector.push_back(polygon_object);
        selected_object = &polygon_vector.back();

        QRect ellipse_in_polygon(x, y, 1, 1);
        polygon_object.ellipse_vector.push_back(ellipse_in_polygon);
        addEllipse(polygon_object.ellipse_vector.back(), QPen(Qt::NoPen), QBrush(Qt::green));
    }
    else {

        if (selected_object->polygon_coordinates.size() < 2) {

            QLineF line_in_polygon(selected_object->polygon_coordinates.back().x(), selected_object->polygon_coordinates.back().y(), x, y);
            selected_object->line_vector.push_back(line_in_polygon);
            addLine(selected_object->line_vector.back(), QPen(Qt::green, 0.1));

            QRect ellipse_in_polygon(x, y, 1.0, 1.0);
            selected_object->ellipse_vector.push_back(ellipse_in_polygon);
            addEllipse(selected_object->ellipse_vector.back(), QPen(Qt::NoPen), QBrush(Qt::green));
            selected_object->polygon_coordinates.push_back(QPointF(x, y));

        }
        else {
            if (selected_object->polygon_coordinates.size() == 2) {

                QLineF pred_line_in_polygon(selected_object->polygon_coordinates.back().x(), selected_object->polygon_coordinates.back().y(), x, y);
                selected_object->line_vector.push_back(pred_line_in_polygon);
                addLine(selected_object->line_vector.back(), QPen(Qt::green, 0.1));

                QRect ellipse_in_polygon(x, y, 1.0, 1.0);
                selected_object->ellipse_vector.push_back(ellipse_in_polygon);
                addEllipse(selected_object->ellipse_vector.back(), QPen(Qt::NoPen), QBrush(Qt::green));
                selected_object->polygon_coordinates.push_back(QPointF(x, y));

                QLineF next_line_in_polygon(x, y, selected_object->polygon_coordinates.front().x(), selected_object->polygon_coordinates.front().y());
                selected_object->line_vector.push_back(next_line_in_polygon);
                addLine(selected_object->line_vector.back(), QPen(Qt::green, 0.1));
            }
            else {

                QGraphicsLineItem line_on_scene(this->itemAt(selected_object->line_vector.back().p1(), QTransform()));
                this->removeItem(line_on_scene.parentItem());
                selected_object->line_vector.pop_back();

                QLineF pred_line_in_polygon(selected_object->polygon_coordinates.back().x(), selected_object->polygon_coordinates.back().y(), x, y);
                selected_object->line_vector.push_back(pred_line_in_polygon);
                addLine(selected_object->line_vector.back(), QPen(Qt::green, 0.1));

                QRect ellipse_in_polygon(x, y, 1.0, 1.0);
                selected_object->ellipse_vector.push_back(ellipse_in_polygon);
                addEllipse(selected_object->ellipse_vector.back(), QPen(Qt::NoPen), QBrush(Qt::green));
                selected_object->polygon_coordinates.push_back(QPointF(x, y));

                QLineF next_line_in_polygon(x, y, selected_object->polygon_coordinates.front().x(), selected_object->polygon_coordinates.front().y());
                selected_object->line_vector.push_back(next_line_in_polygon);
                addLine(selected_object->line_vector.back(), QPen(Qt::green, 0.1));

            }

        }
        addPolygon(polygon_vector.back().polygon_coordinates, QPen(Qt::NoPen), QBrush(Qt::darkGreen));
    }
}
*/

/*
void PaintScene::PolygonDrawing(double x, double y) {
    if (!IsSelected()) {                                                                                                       // Сделать выделение текущего полигона 
        QPolygonF new_polygon;
        new_polygon.push_back(QPointF(x, y));
        polygon_vector.push_back(new_polygon);
        selected_object = &polygon_vector.back();

        
        last_polygon_on_scene = addPolygon(polygon_vector.back(), QPen(Qt::NoPen), QBrush(daph_color));
        addEllipse(QRectF(x-0.5, y-0.5, 1.0, 1.0), QPen(Qt::NoPen), QBrush(Qt::green));    
    }
    else {        
        this->removeItem(last_polygon_on_scene);                                                             //добавление новой точки в полигон
        selected_object->push_back(QPointF(x, y));

        last_polygon_on_scene=addPolygon(*selected_object, QPen(Qt::NoPen), QBrush(daph_color));
        addEllipse(QRectF(x-0.5, y-0.5, 1.0, 1.0), QPen(Qt::NoPen), QBrush(Qt::green));   
    }
}
*/

void PaintScene::AddPointToPolygon(double x, double y) {
    if (!IsSelected()) {                                                                                                       // Сделать выделение текущего полигона 
        CustomPolygon new_polygon;
        new_polygon.push_back(QPointF(x, y));
        polygon_vector.push_back(new_polygon);
        selected_object = &polygon_vector.back();
        polygon_pointer_vector.push_back(&polygon_vector.back());
        count++;
        polygons_on_scene.push_back(addPolygon(polygon_vector.back(), QPen(Qt::NoPen), QBrush(daph_color)));
        selected_object->pointer_vector.push_back(addEllipse(QRectF(x - 0.5, y - 0.5, 1.0, 1.0), QPen(Qt::NoPen), QBrush(Qt::green)));
    }
    else {
        int cur_polygon_index = 0;
        for (int i = 0; i < polygons_on_scene.count(); i++)
        {
            if (*selected_object == polygon_vector[i]) {
                removeItem(polygons_on_scene[i]);
                cur_polygon_index = i;
                break;
            }
        }
        //removeItem(polygons_on_scene.back());                                                             //добавление новой точки в полигон, удаляется старый полигон и рисуется новый с добавленной точкой, то же самое происходит и с точками - вершинами полигона
        selected_object->push_back(QPointF(x, y));
        polygons_on_scene[cur_polygon_index] = addPolygon(*selected_object, QPen(Qt::NoPen), QBrush(daph_color));
        selected_object->pointer_vector.push_back(addEllipse(QRectF(x - 0.5, y - 0.5, 1.0, 1.0), QPen(Qt::NoPen), QBrush(Qt::green)));

        for (int i = 0; i < selected_object->pointer_vector.size(); i++)
        {
            removeItem(selected_object->pointer_vector[i]);
            selected_object->pointer_vector[i] = nullptr;
            selected_object->pointer_vector[i] = addEllipse(QRectF((*selected_object)[i].x() - 0.5, (*selected_object)[i].y() - 0.5, 1.0, 1.0), QPen(Qt::NoPen), QBrush(Qt::green));
        }
    }
}
/*
void PaintScene::RemovePointFromPolygon(double x, double y)
{
    if (IsSelected()) {
        double r = 0.5;
        int selected_point_number = 0;
        for (int i = 0; i < (*selected_object).size(); i++)                             //проверка, что позиция мыши принаджелит эллипсу вокруг точки
        {
            double d = sqrt(pow(x - (*selected_object)[i].x(), 2)+ pow(y - (*selected_object)[i].y(), 2));
            if (d <= r) {
                selected_point_number = i;
                break;
            }
        }
        int selected_polygon_number = 0;
        for (int i = 0; i < polygons_on_scene.size(); i++)
        {
            if (polygon_pointer_vector[i] == selected_object) {
                selected_polygon_number = i;
                break;
            }
        }
        polygon_vector[selected_polygon_number].erase(polygon_vector[selected_polygon_number].begin() + selected_point_number - 1);
        removeItem(polygons_on_scene[selected_polygon_number]);
        polygons_on_scene[selected_polygon_number] = addPolygon(*selected_object, QPen(Qt::NoPen), QBrush(daph_color));
    }
}
*/
void PaintScene::RemovePointFromPolygon(QGraphicsItem* point_for_removing)
{
    if (IsSelected()) {
        for (int i = 0; i < selected_object->pointer_vector.size(); i++)
        {
            if (selected_object->pointer_vector[i] == point_for_removing) {
                removeItem(point_for_removing);
                selected_object->pointer_vector.erase(selected_object->pointer_vector.begin() + i - 1);
                selected_object->point_vector.erase(selected_object->point_vector.begin() + i - 1);
                
                int selected_polygon_number = 0;
                for (int i = 0; i < polygons_on_scene.size(); i++)
                {
                    if (polygon_pointer_vector[i] == selected_object) {
                        selected_polygon_number = i;
                        break;
                    }
                }
                polygon_vector[selected_polygon_number].erase(polygon_vector[selected_polygon_number].begin() + i - 1);
                removeItem(polygons_on_scene[selected_polygon_number]);
                polygons_on_scene[selected_polygon_number] = addPolygon(*selected_object, QPen(Qt::NoPen), QBrush(daph_color));
            }
        }
    }
}

void PaintScene::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (Qt::LeftButton == event->button() && Qt::NoModifier == event->modifiers()) {                                     // Отрисовка точки
        PointDrawing(event->scenePos().x() - 5, event->scenePos().y() - 5);
    }

    if (Qt::ControlModifier == event->modifiers() && Qt::LeftButton == event->button()) {                                  // Отрисовка полигона                                                                                                                 
        AddPointToPolygon(event->scenePos().x(), event->scenePos().y());
    }

    if (Qt::RightButton == event->button() && event->modifiers() == Qt::NoModifier) {                                                                         // Снять выделение с объекта
        if (IsSelected()) {
            selected_object = nullptr;
        }
    }  
    if (Qt::LeftButton == event->button() && event->modifiers()==Qt::NoModifier) {
        current_mouse_pos.setX(event->scenePos().x());
        current_mouse_pos.setY(event->scenePos().y());
        SetSelectedObject(itemAt(current_mouse_pos, QTransform()));
    }
    if (Qt::RightButton == event->button() && Qt::AltModifier == event->modifiers()) {                                     // Удаление точки
        
        RemovePointFromPolygon(itemAt(QPointF(event->scenePos().x(), event->scenePos().y()), QTransform()));
    }
    if (Qt::LeftButton == event->MouseButtonDblClick && Qt::NoModifier == event->modifiers()) {                                     // Удаление точки

        SetSelectedObject(itemAt(QPointF(event->scenePos().x(), event->scenePos().y()), QTransform()));
    }
}


void PaintScene::keyPressEvent(QKeyEvent* keyEvent) 
{
    if (Qt::Key_Delete == keyEvent->key()) {
        current_key_event = keyEvent;

        RemovePointFromPolygon(mouseGrabberItem());
    }
}

void PaintScene::keyReleaseEvent(QKeyEvent* keyEvent)
{
    if (Qt::Key_Delete == keyEvent->key()) {
        current_key_event = nullptr;
    }
}








