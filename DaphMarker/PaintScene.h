//рисование в окне с кадрами
#ifndef PAINTSCENE_H
#define PAINTSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QTimer>
#include <QDebug>
#include <QPointF>
#include <qpolygon.h>
#include <QGraphicsEllipseItem>
#include <QGraphicsItem>
#include <QRectF>
#include <QPainter>
#include <qwidget.h>
#include <qlist.h>
#include <QGraphicsLineItem>
#include <QGraphicsPolygonItem>
#include "CustomPolygon.h"
#include <QColor>
#include <QKeyEvent>

class PaintScene : public QGraphicsScene
{

    Q_OBJECT

public:
    explicit PaintScene(QObject* parent = 0);
    ~PaintScene();
    //QVector<QPointF> point_vector;
    //QVector<QPolygonF> polygon_vector;
    //QVector<QRectF> ellipse_in_polygon_vector;
    //QVector<QLineF> line_in_polygon_vector;
    QVector<CustomPolygon> polygon_vector;
    QVector<CustomPolygon*> polygon_pointer_vector;

    //QVector<QPolygonF> polygon_vector;
    
    QVector<QRectF> single_ellipse_vector;
    void AddPointToPolygon(double x, double y);
    //void RemovePointFromPolygon(double x, double y);
    void RemovePointFromPolygon(QGraphicsItem* point_from_polygon);
    void PointDrawing(double x, double y);
    CustomPolygon* selected_object = nullptr;    //указатель на объект из polygon_vector
    //QPolygonF* selected_object = nullptr;
    QVector<QGraphicsPolygonItem*> polygons_on_scene;
    //QGraphicsPolygonItem* last_polygon_on_scene = nullptr;
    QColor daph_color = QColor(0, 100, 0, 100);
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    QPointF current_mouse_pos;
    void keyPressEvent(QKeyEvent* keyEvent);
    void keyReleaseEvent(QKeyEvent* keyEvent);
    bool IsSelected();
    void SetSelectedObject(QGraphicsItem* new_selected_object);
    QKeyEvent* current_key_event = nullptr;
    int count = 0;
};

#endif // PAINTSCENE_H
