/********************************************************************************
** Form generated from reading UI file 'daphmarker.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DAPHMARKER_H
#define UI_DAPHMARKER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "customview.h"

QT_BEGIN_NAMESPACE

class Ui_daphmarker
{
public:
    QAction *actionOpen;
    QAction *actionNext;
    QAction *actionPrevious;
    QAction *actionOpen_json;
    QWidget *centralwidget;
    CustomView *graphicsView;
    QMenuBar *menubar;
    QMenu *menuFile;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *daphmarker)
    {
        if (daphmarker->objectName().isEmpty())
            daphmarker->setObjectName(QString::fromUtf8("daphmarker"));
        daphmarker->resize(2000, 1400);
        actionOpen = new QAction(daphmarker);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/rec/icons/foldertealopen_93101.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon);
        actionNext = new QAction(daphmarker);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/rec/icons/ic_arrow_forward_128_28222.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNext->setIcon(icon1);
        actionPrevious = new QAction(daphmarker);
        actionPrevious->setObjectName(QString::fromUtf8("actionPrevious"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/rec/icons/ic_arrow_back_128_28226.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrevious->setIcon(icon2);
        actionOpen_json = new QAction(daphmarker);
        actionOpen_json->setObjectName(QString::fromUtf8("actionOpen_json"));
        centralwidget = new QWidget(daphmarker);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        graphicsView = new CustomView(centralwidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(10, 10, 1330, 1074));
        graphicsView->setMinimumSize(QSize(50, 50));
        daphmarker->setCentralWidget(centralwidget);
        menubar = new QMenuBar(daphmarker);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 2000, 21));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        daphmarker->setMenuBar(menubar);
        statusbar = new QStatusBar(daphmarker);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        daphmarker->setStatusBar(statusbar);
        toolBar = new QToolBar(daphmarker);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        daphmarker->addToolBar(Qt::TopToolBarArea, toolBar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionOpen_json);
        toolBar->addAction(actionPrevious);
        toolBar->addAction(actionNext);

        retranslateUi(daphmarker);

        QMetaObject::connectSlotsByName(daphmarker);
    } // setupUi

    void retranslateUi(QMainWindow *daphmarker)
    {
        daphmarker->setWindowTitle(QCoreApplication::translate("daphmarker", "DaphMarker", nullptr));
        actionOpen->setText(QCoreApplication::translate("daphmarker", "Open", nullptr));
#if QT_CONFIG(shortcut)
        actionOpen->setShortcut(QCoreApplication::translate("daphmarker", "Alt+F", nullptr));
#endif // QT_CONFIG(shortcut)
        actionNext->setText(QCoreApplication::translate("daphmarker", "Next", nullptr));
#if QT_CONFIG(tooltip)
        actionNext->setToolTip(QCoreApplication::translate("daphmarker", "Next", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionNext->setShortcut(QCoreApplication::translate("daphmarker", "Right", nullptr));
#endif // QT_CONFIG(shortcut)
        actionPrevious->setText(QCoreApplication::translate("daphmarker", "Previous", nullptr));
#if QT_CONFIG(tooltip)
        actionPrevious->setToolTip(QCoreApplication::translate("daphmarker", "Previous", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionPrevious->setShortcut(QCoreApplication::translate("daphmarker", "Left", nullptr));
#endif // QT_CONFIG(shortcut)
        actionOpen_json->setText(QCoreApplication::translate("daphmarker", "Open json", nullptr));
        menuFile->setTitle(QCoreApplication::translate("daphmarker", "File", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("daphmarker", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class daphmarker: public Ui_daphmarker {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DAPHMARKER_H
