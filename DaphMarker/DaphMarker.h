#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
#include "ui_daphmarker.h"
#include <QtCore>
#include <QtGui>
#include <QGraphicsItem>
#include <qgraphicsscene.h>
#include <QtGui>
#include <qdir.h> 
#include <qstring.h>
#include <qfile.h>
#include <qjsonobject.h>
#include <QJsonParseError>
#include <qfiledialog.h>
#include <qjsonarray.h>
#include "PaintScene.h"
#include <QMouseEvent>
#include <qvector.h>
#include <QPointF>
#include <qpolygon.h>

QT_BEGIN_NAMESPACE
namespace Ui { class daphmarker; }
QT_END_NAMESPACE

class daphmarker : public QMainWindow, public Ui::daphmarker
{
    Q_OBJECT

public:
    daphmarker(QWidget *parent = nullptr);
    ~daphmarker();
    
    
private slots:
    void on_actionOpen_triggered();

    void on_actionNext_triggered();

    void on_actionPrevious_triggered();
    
    void on_actionOpen_json_triggered();

public:
    Ui::daphmarker *ui;
    //QGraphicsScene *main_scene;
    PaintScene* main_scene;
    // ��� ������
    QString current_file_name;
    QDir current_dir;
    QStringList cur_files_list;
    QList<QString>::const_iterator cur_files_it;
    //��� json
    QFile json_file;
    QJsonDocument json_doc;
    QJsonArray polygons_json_array;
    QVector<QVector<QPair<double, double>>> polygons_array;
    QJsonParseError json_doc_error;
    QString current_json;
    QDir current_json_dir;
    QStringList cur_json_list;
    QList<QString>::const_iterator cur_json_it;
    
       
};
#endif // IMAGEVIEWER_H
 