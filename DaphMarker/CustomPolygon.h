#ifndef CUSTOMPOLYGON_H
#define CUSTOMPOLYGON_H

#include <qgraphicsview.h>
#include <QPolygonF>
#include <QVector>
#include <QtGui>
#include <QRectF>
#include <QLineF>
#include <QPointF>
#include <vector>
#include <QGraphicsItem>
#include <qpolygon.h>

class CustomPolygon : public QPolygonF {
	
public:
	QVector<QRectF> point_vector;
	QVector<QGraphicsEllipseItem*> pointer_vector;
};

#endif // CUSTOMPOLYGON_H
