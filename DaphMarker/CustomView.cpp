//������ � ������� ����������� ������
#include "CustomView.h"

CustomView::CustomView(QWidget *parent) : QGraphicsView(parent) {

}
void CustomView::wheelEvent(QWheelEvent* event) {
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	double scale_factor = 1.15;
	if (event->delta() > 0) {
		scale(scale_factor, scale_factor);
	}
	else {
		scale(1 / scale_factor, 1 / scale_factor);
	}
}