#include "DaphMarker.h"
#include "ui_daphmarker.h"
#include "PaintScene.h"
#include <QFileDialog>
#include <qgraphicsview.h>
#include <qgraphicsscene.h>
#include <qdialog.h>
#include <QGraphicsItem>
#include <qdiriterator.h>

daphmarker::daphmarker(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::daphmarker) {
    ui->setupUi(this);
    //main_scene = new QGraphicsScene(this);
    main_scene = new PaintScene(this);
    main_scene->setSceneRect(0, 0, 1280, 1024); //размер сцены как размер кадра
    ui->graphicsView->setScene(main_scene);
}

daphmarker::~daphmarker() {
    delete ui;
}

// открытие кадра
void daphmarker::on_actionOpen_triggered() {
    QFileDialog dialog(this, "Open file", "*.png");
    if (dialog.exec()) {
        current_dir = dialog.directory();
        cur_files_list = current_dir.entryList(QList<QString>() << "*.png", QDir::Files);
        for (QList<QString>::iterator it = cur_files_list.begin(); it < cur_files_list.end(); it++) {
            QString plus_dir = current_dir.absolutePath() + "/" + *it;
            *it = plus_dir;
        }
        current_file_name = dialog.selectedFiles().first();
        cur_files_it = cur_files_list.constBegin();
        while (current_file_name != QString(*cur_files_it)) {
            cur_files_it++;
        }
        main_scene->clear();
        main_scene->single_ellipse_vector.clear();
        main_scene->polygon_vector.clear();
        main_scene->selected_object = nullptr;
        main_scene->addPixmap(QPixmap(current_file_name));
    }
}

// следующий кадр
void daphmarker::on_actionNext_triggered() {
    if (current_dir!=QDir(".")) {
        main_scene->clear();
        main_scene->single_ellipse_vector.clear();
        main_scene->polygon_vector.clear();
        main_scene->selected_object = nullptr;
        if (cur_files_it != cur_files_list.constEnd() - 1)
            cur_files_it++;
        else
            cur_files_it = cur_files_list.constBegin();
        current_file_name = *cur_files_it;
        main_scene->addPixmap(QPixmap(current_file_name));
    }
}
// предыдущий кадр
void daphmarker::on_actionPrevious_triggered() {
    if (current_dir != QDir(".")) {
        main_scene->clear();
        main_scene->single_ellipse_vector.clear();
        main_scene->polygon_vector.clear();
        main_scene->selected_object = nullptr;
        if (cur_files_it != cur_files_list.constBegin())
            cur_files_it--;
        else
            cur_files_it = cur_files_list.constEnd() - 1;
        current_file_name = *cur_files_it;
        main_scene->addPixmap(QPixmap(current_file_name));
    }
}
// открытие json файла
/*
void daphmarker::on_actionOpen_json_triggered()
{
    QFileDialog dialog(this, "Open file", "*.json");
    if (dialog.exec()) {
        polygons_array.clear();
        current_json_dir = dialog.directory();
        cur_json_list = current_json_dir.entryList(QList<QString>() << "*.json", QDir::Files);
        for (QList<QString>::iterator it = cur_json_list.begin(); it < cur_json_list.end(); it++) {
            QString plus_dir = current_json_dir.absolutePath() + "/" + *it;
            *it = plus_dir;
        }
        current_json = dialog.selectedFiles().first();
        cur_json_it = cur_json_list.constBegin();
        while (current_json != QString(*cur_json_it)) {
            cur_json_it++;
        }
        json_file.setFileName(current_json);
        
        if (json_file.open(QIODevice::ReadWrite|QFile::Text)) {
            json_doc = QJsonDocument::fromJson(QByteArray(json_file.readAll()));
        };
        json_file.close();
        polygons_json_array = QJsonValue(json_doc.object().value("shapes")).toArray();             // считывание данных из json
        for (int i = 0; i < polygons_json_array.count(); i++)
        {
            QJsonArray polygons_points_array;
            polygons_points_array = QJsonValue(polygons_json_array.at(i).toObject().value("points")).toArray();
            QVector<QPair<double, double>> points;
            for (int j = 0; j < polygons_points_array.count(); j++)
            {
                QPair<double, double> coordinates;
                QJsonArray x_y = polygons_points_array.at(j).toArray();
                coordinates.first = x_y.at(0).toDouble();
                coordinates.second = x_y.at(1).toDouble();
                points.push_back(coordinates);
            }
            polygons_array.push_back(points);
        }
        for (int i = 0; i < polygons_array.size(); i++)                           // отрисовка считанных данных и их сохранение в polygon_vector 
        {
            CustomPolygon polygon_from_json;
            for (int j = 0; j < polygons_array[i].size(); j++)
            {
                main_scene->PolygonDrawing(polygons_array[i][j].first, polygons_array[i][j].second);
                polygon_from_json.polygon_coordinates.push_back(QPointF(polygons_array[i][j].first, polygons_array[i][j].second));
                polygon_from_json.ellipse_vector.push_back(QRectF(polygons_array[i][j].first, polygons_array[i][j].second, 1.0, 1.0));
                if (j != 0) {
                    polygon_from_json.line_vector.push_back(QLineF(polygon_from_json.polygon_coordinates.back().x(), 
                        polygon_from_json.polygon_coordinates.back().y(),
                        polygons_array[i][j].first, 
                        polygons_array[i][j].second));
                }
            }
            main_scene->polygon_vector.push_back(polygon_from_json);
            main_scene->selected_object = nullptr;
        }
    }
}
*/

void daphmarker::on_actionOpen_json_triggered()
{
    QFileDialog dialog(this, "Open file", "*.json");
    if (dialog.exec()) {
        polygons_array.clear();
        current_json_dir = dialog.directory();
        cur_json_list = current_json_dir.entryList(QList<QString>() << "*.json", QDir::Files);
        for (QList<QString>::iterator it = cur_json_list.begin(); it < cur_json_list.end(); it++) {
            QString plus_dir = current_json_dir.absolutePath() + "/" + *it;
            *it = plus_dir;
        }
        current_json = dialog.selectedFiles().first();
        cur_json_it = cur_json_list.constBegin();
        while (current_json != QString(*cur_json_it)) {
            cur_json_it++;
        }
        json_file.setFileName(current_json);

        if (json_file.open(QIODevice::ReadWrite | QFile::Text)) {
            json_doc = QJsonDocument::fromJson(QByteArray(json_file.readAll()));
        };
        json_file.close();
        polygons_json_array = QJsonValue(json_doc.object().value("shapes")).toArray();             // считывание данных из json
        for (int i = 0; i < polygons_json_array.count(); i++)
        {
            QJsonArray polygons_points_array;
            polygons_points_array = QJsonValue(polygons_json_array.at(i).toObject().value("points")).toArray();
            QVector<QPair<double, double>> points;
            for (int j = 0; j < polygons_points_array.count(); j++)
            {
                QPair<double, double> coordinates;
                QJsonArray x_y = polygons_points_array.at(j).toArray();
                coordinates.first = x_y.at(0).toDouble();
                coordinates.second = x_y.at(1).toDouble();
                points.push_back(coordinates);
            }
            polygons_array.push_back(points);
        }
        for (int i = 0; i < polygons_array.size(); i++)                           // отрисовка считанных данных и их сохранение в polygon_vector 
        {
            //QPolygonF polygon_from_json;
            CustomPolygon polygon_from_json;
            for (int j = 0; j < polygons_array[i].size(); j++)
            {
                main_scene->AddPointToPolygon(polygons_array[i][j].first, polygons_array[i][j].second);
                //polygon_from_json.push_back(QPointF(polygons_array[i][j].first, polygons_array[i][j].second));
            }
            //main_scene->polygon_vector.push_back(polygon_from_json);
            main_scene->selected_object = nullptr;
        }
    }
}

